#!/usr/bin/env python
# -*- coding: utf-8 -*-

#from requests import async
import requests
import json
import time

class Cantonfair:
    def __init__(self):
        pass
    def obtenerProductosCategoria(self):
        url = "https://gateway.cantonfair.org.cn/Api/ESAPI/product/classify?page=0&size=15&type=undefined"
        payload = "{\"orderModel\": {\"order\": \"asc\",\"properties\": []},\"page\": 0,\"searchKeys\":[\"productNameCN\", \"productNameEN\"],\"searchValue\": \"\",\"selectAndMap\":{\"boothAreaSearch\":[\"HD06\"],\"boothNumber\":[],\"categoryId\":[\"0798d7c0f5c046ebba6f32c3debd8319\"]},\"size\": 15}"
        headers= {}
        response = requests.request("POST", url, headers=headers, data = payload)
        print(response.text.encode('utf8'))


    def obtenerSellersPorCategoria(self, value):
        
        listado_completo = []
        contador = 0
        for numeroIndex in range(value['pageSize']):

            try:
                cookies = {
                    'insight_quid': 'Tjdf_Njs6_',
                    'sensorsdata2015jssdkcross': '%7B%22distinct_id%22%3A%2217546b5ee201c9-0393c50623d9c2-7f2c6753-1049088-17546b5ee212fb%22%2C%22first_id%22%3A%22%22%2C%22props%22%3A%7B%22%24latest_traffic_source_type%22%3A%22%E7%9B%B4%E6%8E%A5%E6%B5%81%E9%87%8F%22%2C%22%24latest_search_keyword%22%3A%22%E6%9C%AA%E5%8F%96%E5%88%B0%E5%80%BC_%E7%9B%B4%E6%8E%A5%E6%89%93%E5%BC%80%22%2C%22%24latest_referrer%22%3A%22%22%7D%2C%22%24device_id%22%3A%2217546b5ee201c9-0393c50623d9c2-7f2c6753-1049088-17546b5ee212fb%22%7D',
                    'sajssdk_2015_cross_new_user': '1',
                    'cookieAccepted': '1',
                }

                headers = {
                    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0',
                    'Accept': '/',
                    'Accept-Language': 'es-CL',
                    'Content-Type': 'application/json',
                    'Origin': 'https://www.cantonfair.org.cn',
                    'Connection': 'keep-alive',
                    'Referer': value['Referer'],
                    'Cache-Control': 'max-age=0',
                    'TE': 'Trailers',
                }

                data = '{"area":{"china":0,"other":0},"busType":{"isProduct":0,"isExport":0,"other":0},"companyType":{"isBrand":0,"isGreen":0,"isInvite":0,"isAeo":0,"isCf":0,"isPoorSpecial":0},"history":{"isFirst":0,"isSecond":0},"productType":{"isNew":0,"isCf":0,"domesticSales":0},"tradeType":{"isOem":0,"isOBM":0,"isCDM":0},"lang":"en","order":{"relevant":1},"pageIndex":'+str(numeroIndex)+',"searchType":"keyword","pageSize":40,"sessionId":"1eaf0ac3-502e-45b1-a06b-ca90ea9ce63d","keyword":"","category":'+str(value['CatNum'])+',"isRecommend":1}'
                response = requests.post('https://www.cantonfair.org.cn/en/api/exhibitsSreach/getSearchCompany', headers=headers, cookies=cookies, data=data)
                data = response.json()
                #print(str(numeroIndexvalue) +' de '+ str(value['pageSize']))

                listado_completo.append(data)
            
            except:
                print("error")
        with open(str(value['CategoryName'])+'.json', 'w', encoding='utf-8') as json_file:
            json.dump(listado_completo, json_file, sort_keys=False, indent=2,ensure_ascii=False)
            #logging.info('Guardado archivo ::' + str(self.id_metadata))


    def obtenerListaProductosPorSeller(self, companyId):
        ## OK
        import requests

        url = "https://gateway.cantonfair.org.cn/api/clientMicroSiteAggregator/cms/exhibits/list?lang=en&pageSize=48&pageNo=1&exhibitorId="+ companyId

        payload = {}
        headers = {
          'Authorization': 'Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IjE2RjNFNURDNUJDNTEyMEUxM0M0ODk2MDBBNjNFMDFGMzBCMkM3QzAiLCJ0eXAiOiJKV1QiLCJ4NXQiOiJGdlBsM0Z2RkVnNFR4SWxnQ21QZ0h6Q3l4OEEifQ.eyJuYmYiOjE1OTI0MzYyNjUsImV4cCI6MTU5MjQzOTg2NSwiaXNzIjoiaHR0cHM6Ly9hdXRoLmNhbnRvbmZhaXIub3JnLmNuIiwiYXVkIjpbImh0dHBzOi8vYXV0aC5jYW50b25mYWlyLm9yZy5jbi9yZXNvdXJjZXMiLCJBcGlHYXRld2F5Il0sImNsaWVudF9pZCI6IkV4aGliaXRvclNpdGUiLCJzdWIiOiI2NDZhMDAwMC0wMGFhLTUyNTQtOWU0Yi0wOGQ4MTE0NjVjMmYiLCJhdXRoX3RpbWUiOjE1OTI0MzEwMjcsImlkcCI6ImxvY2FsIiwibmFtZSI6IkFiZWwgTWVqaWFzIiwibG9naW5Vc2VySWQiOiI2NDZhMDAwMC0wMGFhLTUyNTQtOWU0Yi0wOGQ4MTE0NjVjMmYiLCJlbWFpbCI6IkFCRUwuTUVKSUFTQENMVUJPRkVSVEEuQ0wiLCJtb2JpbGUiOiIiLCJ0b3BPcmdJZCI6IjkyNTkwMDAwLTAwMjUtNTI1NC01MmM5LTA4ZDgxMTQ2YzIwMiIsInVzZXJJZCI6IjY0NmEwMDAwLTAwYWEtNTI1NC05ZTQ2LTA4ZDgxMTQ2NWMyZiIsImZ1bGxOYW1lIjoiQWJlbCBNZWppYXMiLCJzY29wZSI6WyJvcGVuaWQiLCJBcGlHYXRld2F5Iiwib2ZmbGluZV9hY2Nlc3MiXSwiYW1yIjpbInB3ZCJdfQ.dFQddIdMYkmU1h_4J5QS0GDw65wfFiGZOTAFkdSd83268v2htVqUT9IC1Y8ad3kWeMMYutCKWr5-L_h8G3M7lKktZgg1XETJdCrALrK816re08eFykc62fYvEHHEui0QlYN4jwL7TQ05YPDtxwiZyqHk2l94r2Q7I2cuxGp1ZjQApE4CsU2Ko8F0GfK1rtkgeRQbRTYj_Pu9RnMqPd-huaelnqGgnpk8euh2oMqJK0toTFcqtlSvmNc2NRcssLB4i9tU9oy3_C8tRhR_kwlrYF58h2JjLWyOVShTCvHVO30xdL9kqDfe2ski21YfVqippskK5OmPwkP16BHR3eUjiQ'
        }

        response = requests.request("GET", url, headers=headers, data = payload)

        return response.json()


    def obtenerInformacionPorSeller(self, companyId):
        url = "https://gateway.cantonfair.org.cn/api/clientMicroSiteAggregator/cms/exhibitor/detail?lang=en&siteId=siteExhibitorDetail&id=" + companyId

        payload = {}
        headers = {
          'Authorization': 'Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IjE2RjNFNURDNUJDNTEyMEUxM0M0ODk2MDBBNjNFMDFGMzBCMkM3QzAiLCJ0eXAiOiJKV1QiLCJ4NXQiOiJGdlBsM0Z2RkVnNFR4SWxnQ21QZ0h6Q3l4OEEifQ.eyJuYmYiOjE1OTI1NzcwMjcsImV4cCI6MTU5MjU4MDYyNywiaXNzIjoiaHR0cHM6Ly9hdXRoLmNhbnRvbmZhaXIub3JnLmNuIiwiYXVkIjpbImh0dHBzOi8vYXV0aC5jYW50b25mYWlyLm9yZy5jbi9yZXNvdXJjZXMiLCJBcGlHYXRld2F5Il0sImNsaWVudF9pZCI6IkV4aGliaXRvclNpdGUiLCJzdWIiOiI2NDZhMDAwMC0wMGFhLTUyNTQtOWU0Yi0wOGQ4MTE0NjVjMmYiLCJhdXRoX3RpbWUiOjE1OTI1NzcwMjMsImlkcCI6ImxvY2FsIiwibmFtZSI6IkFiZWwgTWVqaWFzIiwibG9naW5Vc2VySWQiOiI2NDZhMDAwMC0wMGFhLTUyNTQtOWU0Yi0wOGQ4MTE0NjVjMmYiLCJlbWFpbCI6IkFCRUwuTUVKSUFTQENMVUJPRkVSVEEuQ0wiLCJtb2JpbGUiOiIiLCJ0b3BPcmdJZCI6IjkyNTkwMDAwLTAwMjUtNTI1NC01MmM5LTA4ZDgxMTQ2YzIwMiIsInVzZXJJZCI6IjY0NmEwMDAwLTAwYWEtNTI1NC05ZTQ2LTA4ZDgxMTQ2NWMyZiIsImZ1bGxOYW1lIjoiQWJlbCBNZWppYXMiLCJzY29wZSI6WyJvcGVuaWQiLCJBcGlHYXRld2F5Iiwib2ZmbGluZV9hY2Nlc3MiXSwiYW1yIjpbInB3ZCJdfQ.Rot8xFft15sghRW1h7Sj65E_od_DMtSR9TqPKVV4bHZAaA77fW6aIK_k3ClGdBChNBPX5HyJO67bPXmKWGdzDZx8VmplWTBdDy4LfNk5E2NvcjPiQvWeH8bL_Kfcg0Zpz5IvTYQkFOp1ck33clLiguglbh4lcTA99d0JVphu_Bv-abIrdXDpYquMbdg-VJXAkkGgO0qd7vc-VtQQk7NV57Sayzwy8dzOdMgKVtXoyeP4UzHh2YEm6K9fN9iwRqyZXmCTg1_3t-7_52nQwjGYqQkYiuMPEtWEtHjirZUULr8lEfnHV-L6BVZylHDIkdjOMKZYBGO3mFBO_PcRLPzflw'
        }

        response = requests.request("GET", url, headers=headers, data = payload)

        print(response.text.encode('utf8'))




    def obtenerInformacionPorSellerContacto(self, companyId):
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0',
            'Accept': 'application/json, text/plain, */*',
            'Accept-Language': 'es-CL,es;q=0.8,en-US;q=0.5,en;q=0.3',
            'ignoreAuth': 'false',
            'ignoreError': 'true',
            'X-Requested-With': 'XMLHttpRequest',
            'lang': 'en',
            'x-skey': 'hran0uS4WymCBi2vMlbAowhgcCWhN0dV',
            'x-openid': 'eab0f995eba48e9a9cc398902fcf8859',
            'x-uid': '46dccdb6-df82-4bc6-af4b-63d3038b161c',
            'x-userId': '46dccdb6-df82-4bc6-af4b-63d3038b161c',
            'x-loginUserId': '02c6ce97-faf6-40b3-812c-4d13f06ec9a2',
            'Origin': 'https://ex.cantonfair.org.cn',
            'Connection': 'keep-alive',
            'Referer': 'https://ex.cantonfair.org.cn/pc/en/exhibitor/'+companyId+'/contact?lang=en&',
        }

        params = (
            ('lang', 'en'),
            ('id', companyId),
        )

        response = requests.get('https://gateway.cantonfair.org.cn/api/clientMicroSiteAggregator/cms/exhibitor/contact', headers=headers, params=params)


        #print(response.json())
        return response.json()