import asyncio
import aiohttp
import time
import json

respuestas = []


async def get(companyId):
    try:
        async with aiohttp.ClientSession() as session:
            headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0',
            'Accept': 'application/json, text/plain, */*',
            'Accept-Language': 'es-CL,es;q=0.8,en-US;q=0.5,en;q=0.3',
            'ignoreAuth': 'false',
            'ignoreError': 'true',
            'X-Requested-With': 'XMLHttpRequest',
            'lang': 'en',
            'x-skey': 'hran0uS4WymCBi2vMlbAowhgcCWhN0dV',
            'x-openid': 'eab0f995eba48e9a9cc398902fcf8859',
            'x-uid': '46dccdb6-df82-4bc6-af4b-63d3038b161c',
            'x-userId': '46dccdb6-df82-4bc6-af4b-63d3038b161c',
            'x-loginUserId': '02c6ce97-faf6-40b3-812c-4d13f06ec9a2',
            'Origin': 'https://ex.cantonfair.org.cn',
            'Connection': 'keep-alive',
            'Referer': 'https://ex.cantonfair.org.cn/pc/en/exhibitor/'+companyId+'/contact?lang=en&',
            }  
            
            params = (
                ('lang', 'en'),
                ('id', companyId),
            )

            url = 'https://gateway.cantonfair.org.cn/api/clientMicroSiteAggregator/cms/exhibitor/contact'

            async with session.post(url=url, headers=headers, params=params) as response:
      
                resp = await response.read()
                print("headers ::::",headers)
                print("params ::::",params)
                print(response.status)
                print(response)
                #print("Response ::",resp.json())
                respuestas.append(response)

                print("Obtuve correctamente la URL {} con una respuesta de longitud {}.".format(url, len(resp)))
    except Exception as e:
        print("No se puede obtener la URL {} debido a {}.".format(url, e.__class__))


async def main(urls):
    ret = await asyncio.gather(*[get(url) for url in urls])
    print("Finalizado todo. ret es una lista de salidas len {}.".format(len(ret)))


urls = ['4ab00000-005f-5254-40c0-08d7ed77b62e',
        '4ab00000-005f-5254-40c0-08d7ed77b62e']


amount = len(urls)

#start = time.time()
asyncio.get_event_loop().run_until_complete(main(urls))

#for a in respuestas:
#    print(a)

#end = time.time()

#print("Took {} seconds to pull {} websites.".format(end - start, amount))