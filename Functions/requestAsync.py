import aiohttp
import asyncio




async def fetch(session, companyId):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0',
        'Accept': 'application/json, text/plain, */*',
        'Accept-Language': 'es-CL,es;q=0.8,en-US;q=0.5,en;q=0.3',
        'ignoreAuth': 'false',
        'ignoreError': 'true',
        'X-Requested-With': 'XMLHttpRequest',
        'lang': 'en',
        'x-skey': '5Jf70YU8xB2f0r6ap85WecFVBeTvnxfe',
        'x-openid': 'eab0f995eba48e9a9cc398902fcf8859',
        'x-uid': '46dccdb6-df82-4bc6-af4b-63d3038b161c',
        'x-userId': '46dccdb6-df82-4bc6-af4b-63d3038b161c',
        'x-loginUserId': '02c6ce97-faf6-40b3-812c-4d13f06ec9a2',
        'Origin': 'https://ex.cantonfair.org.cn',
        'Connection': 'keep-alive',
        'Referer': 'https://ex.cantonfair.org.cn/pc/en/exhibitor/'+companyId+'/contact',
    }

    params = (
        ('lang', 'en'),
        ('id', companyId),
    )
    async with session.get('https://gateway.cantonfair.org.cn/api/clientMicroSiteAggregator/cms/exhibitor/contact', headers=headers, params=params) as response:
        return await response.text()

async def main():
    urls = [
            '4ab00000-005f-5254-40c0-08d7ed77b62e',
            '4ab00000-005f-5254-40c0-08d7ed77b62e'
        ]
    tasks = []
    async with aiohttp.ClientSession() as session:
        for url in urls:
            tasks.append(fetch(session, url))
        htmls = await asyncio.gather(*tasks)
        for html in htmls:
            print(html[:500])

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())