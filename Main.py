## Concurrent
import time
import concurrent.futures
import urllib.request
import sys
import asyncio

import sys


## Functions
#from Functions import CantonFair
#from Functions import LecturaJson
#from Models import Mongo

# System functions


#CantonFair.Cantonfair().obtenerInformacionPorSeller("4ab00000-005f-5254-6535-08d7ed7826d4")
#CantonFair.Cantonfair().obtenerInformacionPorSeller("4ab00000-005f-5254-6535-08d7ed7826d4")
#CantonFair.Cantonfair().obtenerListaProductosPorSeller("4ab00000-005f-5254-ed8e-08d7ed7bcc0f")

#proceso = CantonFair.Cantonfair()

#LecturaJson.LecturaJson().lecturaArchivosCarpeta()

from Functions import Proceso

#Proceso.Proceso().procesar()
Proceso.Proceso().enviarEmail()


async def run_blocking_tasks(executor, lista):
    loop = asyncio.get_event_loop()
    blocking_tasks = [
        loop.run_in_executor(executor, proceso.obtenerSellersPorCategoria, valor)
        for valor in lista
    ]
    await asyncio.wait(blocking_tasks)


#try:
#    while True:
#        #x = range(1,438)
#        ##print(x)
#        #lista = list(x)
#        #print(lista)
#        lista = [{
#		        'CategoryName': 'sporst demo',
#		        'CatNum':430,
#		        'Referer':'https://www.cantonfair.org.cn/en/exhibitssearch?430&title=%E4%BD%93%E8%82%B2%E5%8F%8A%E6%97%85%E6%B8%B8%E4%BC%91%E9%97%B2%E7%94%A8%E5%93%81&titleEn=Sports%2C%20travel%20and%20recreation%20products&&&',
#                'pageSize':19
#    	        }]

#        executor = concurrent.futures.ThreadPoolExecutor(max_workers=10)
#        event_loop = asyncio.get_event_loop()
        
#        try:
#            event_loop.run_until_complete(run_blocking_tasks(executor, lista))   
#        #event_loop.close()
#        finally:
#            event_loop.close()
#            sys.exit("salida")

#except KeyboardInterrupt:
#    # servicio.Stop()
#    #logging.info('saliendo de Proceso')
#    print("asd")

