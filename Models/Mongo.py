import pymongo


class Mongo:
    def __init__(self):
        self.myclient = None
        self.mydb = None
        self.mycol = None
        self.configurar()
        

    def configurar(self):
        self.myclient = pymongo.MongoClient("mongodb://localhost:27017/")
        self.mydb = self.myclient["cantonfair"]
        self.mycol = self.mydb["bicycles"]


    def test(self):
        mydict = { "name": "John", "address": "Highway 37" }
        x = self.mycol.insert_one(mydict)
        print("valor insertado")

    def insertarContactoBySubCategory(self, json, nombreColeccion):
        self.mycol = self.mydb[nombreColeccion]


        x = self.mycol.insert_one(json)
        print("valor insertado")


    def seleccionarPorCategoria(self, nombreColeccion):
        self.mycol = self.mydb[nombreColeccion]

        cursor = self.mycol.find({},{'id':1, 'name': 1 ,'email': 1})
        listado = []


        for document in cursor:
            #print(document)
            listado.append(document)
        print("resultado obtenido")
        return listado
